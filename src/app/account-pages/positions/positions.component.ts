import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AppService } from 'src/app/app.service';
import { DeletePositionComponent } from 'src/app/shared/delete-position/delete-position.component';

@Component({
  selector: 'app-positions',
  templateUrl: './positions.component.html',
  styleUrls: ['./positions.component.scss']
})
export class PositionsComponent implements OnInit {

  show1: boolean = false;
  show2: boolean = false;
  show11: boolean = false;
  show22: boolean = false
  constructor(
              private _matDialog: MatDialog
  ) { }

  ngOnInit(): void {
  }
  openDeleteDialog() { 
    const dialogRef = this._matDialog.open(DeletePositionComponent, { 

    }); 
    dialogRef.afterClosed().subscribe((response) => { 
      if(response) { 
        
      }
    })

  }
}
