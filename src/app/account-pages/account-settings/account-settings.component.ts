import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AppService } from 'src/app/app.service';
import { CloseAccountComponent } from 'src/app/shared/close-account/close-account.component';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {

  show: boolean = false;
  show1: boolean = false;
  show2: boolean = false;
  hide : boolean = true;
  hide1 : boolean = false;
  hide2 : boolean = true;
  
  constructor(private dialog: MatDialog, private _appService : AppService) { 
    this._appService.titleChange.next("Account Settings")
  }

  ngOnInit(): void {
  }
  
  myFunction() {
    this.hide = !this.hide;
    this.show = !this.show
  }

  myFunction1() {
    this.hide1 = !this.hide1;
    this.show1 = !this.show1
  }

  myFunction2() {
    this.hide2 = !this.hide2;
    this.show2 = !this.show2
  }
  openDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    // dialogConfig.data = {
    //   id: 1,
    //   title: 'Angular For Beginners'
    // };

    let dialogWidth: string;
    let dialogHeight: string;
    let positionBottom: string;
  
    if (window.innerWidth < 768) {
      dialogWidth = '100%';
      dialogHeight = '100%'; 
      positionBottom = '0px';
     
    } else {
      dialogWidth = '780px';
      dialogHeight = '100%'; 
      positionBottom = '';
    }
  
    const dialogRef = this.dialog.open(CloseAccountComponent, {
      height: dialogWidth,
      width: dialogHeight,
      maxWidth: '600px',
      maxHeight: '50vh',
      panelClass: 'custom-modalbox',
      position: {
        bottom: positionBottom,
      }
      
    });
  
  }

}
