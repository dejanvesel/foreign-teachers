import { Component, OnInit } from '@angular/core';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
@Component({
  selector: 'app-my-posts',
  templateUrl: './my-posts.component.html',
  styleUrls: ['./my-posts.component.scss']
})
export class MyPostsComponent implements OnInit {

  declarations: [
    MatSlideToggleModule
  ]
  imports: [
    MatSlideToggleModule
  ]
  exports: [
    MatSlideToggleModule

  ]

  constructor() { }

  ngOnInit(): void {
  }

}
