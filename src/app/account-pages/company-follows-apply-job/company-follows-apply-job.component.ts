import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-company-follows-apply-job',
  templateUrl: './company-follows-apply-job.component.html',
  styleUrls: ['./company-follows-apply-job.component.scss']
})
export class CompanyFollowsApplyJobComponent implements OnInit {
  
  show1: boolean = false;
  totalJobs = false;

  constructor(private _appService : AppService) { 
    this._appService.titleChange.next("Company Follows & Apply Job")
  }

  ngOnInit(): void {
  }

  onClickFollows(){
    this.totalJobs = true;
  }
}
