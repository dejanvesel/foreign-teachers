import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { ApplicationsComponent } from './applications/applications.component';
import { CompanyFollowsApplyJobComponent } from './company-follows-apply-job/company-follows-apply-job.component';
import { MyPostsComponent } from './my-posts/my-posts.component';
import { PositionsComponent } from './positions/positions.component';
import { TeacherTabsGroupComponent } from './teacher-tabs-group/teacher-tabs-group.component';
import { TeacherFollowsContactComponent } from './teacher-follows-contact/teacher-follows-contact.component';
import { FollowsApplicationsComponent } from './follows-applications/follows-applications.component';
const routes: Routes = [
  {
    path: 'account-settings',
    component: AccountSettingsComponent
  },
  {
    path: 'applications',
    component: ApplicationsComponent
  },
  {
    path: 'company-follows-apply-job',
    component: CompanyFollowsApplyJobComponent
  },
  {
    path: 'follows-applications',
    component: FollowsApplicationsComponent
  },
  {
    path: 'my-posts',
    component: MyPostsComponent
  },
  {
    path: 'positions',
    component: PositionsComponent
  },
  {
    path: 'teacher-follows-contact',
    component: TeacherTabsGroupComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountPagesRoutingModule { }
