import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountPagesRoutingModule } from './account-pages-routing.module';
import { TeacherFollowsContactComponent } from './teacher-follows-contact/teacher-follows-contact.component';
import { ApplicationsComponent } from './applications/applications.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { MyPostsComponent } from './my-posts/my-posts.component';
import { CompanyFollowsApplyJobComponent } from './company-follows-apply-job/company-follows-apply-job.component';
import { PositionsComponent } from './positions/positions.component';
import { SharedModule } from '../shared/shared.module';
import { TeacherTabsGroupComponent } from './teacher-tabs-group/teacher-tabs-group.component';
import { TeacherFollowsComponent } from './teacher-tabs-group/teacher-follows/teacher-follows.component';
import { TeacherContactsComponent } from './teacher-tabs-group/teacher-contacts/teacher-contacts.component';

import { FollowsApplicationsComponent } from './follows-applications/follows-applications.component';
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
  declarations: [
    TeacherFollowsContactComponent,
    ApplicationsComponent,
    AccountSettingsComponent,
    MyPostsComponent,
    CompanyFollowsApplyJobComponent,
    PositionsComponent,
    TeacherTabsGroupComponent,
    TeacherFollowsComponent,
    TeacherContactsComponent,
    FollowsApplicationsComponent,
  
  ],
  imports: [
    CommonModule,
    AccountPagesRoutingModule,
    SharedModule,
    MatTabsModule
  ]
})
export class AccountPagesModule { }
