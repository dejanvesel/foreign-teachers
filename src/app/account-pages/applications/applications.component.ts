import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { CompanyService } from 'src/app/company/company.service';
import { CancelApplicationComponent } from 'src/app/shared/cancel-application/cancel-application.component';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss']
})
export class ApplicationsComponent implements OnInit {

  public mobMenu: boolean = true;
  public show: boolean = true;
  public show1: boolean = false;
  private _unsubscribeAll: Subject<void> = new Subject();
  public jobs: any;
 
  constructor(private dialog: MatDialog, private _authService : AuthService, private _companyService : CompanyService) { }

  ngOnInit(): void {
    this._authService.activeUserData.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      data => {
        if (data) {
          this.getBriefJobs();
        }
      }
    );
  }

  getBriefJobs() {
    this._companyService.getBriefJobs().subscribe(
      data => {
        console.log("Get Brief Jobs",data)
        this.jobs = data;
        this.jobs = this.jobs.data.list
    })
  }

  openCancelDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    let dialogWidth: string;
    let dialogHeight: string;

    if (window.innerWidth < 768) {
      dialogWidth = '100%';
      dialogHeight = '100%';
    } else {
      dialogWidth = '765px';
      dialogHeight = '75%';
    }

    const dialogRef = this.dialog.open(CancelApplicationComponent, {
      height: dialogWidth,
      width: dialogHeight,
      maxWidth: '600px',
      maxHeight: dialogHeight,
      panelClass: 'custom-modalbox'
    });
  }

  toggleMobMenu() {
    this.mobMenu = !this.mobMenu;
    this.show = !this.show
  }

  close() {
    this.mobMenu = true;
    this.show = !this.show
  }
}
