export class Resume {
    avator ?: string;
    name ?: string;
    email ?: string;
    brief_intro ?: string;
    experence?: Array<experence>;
    education?: Array<education>;
    certificate ?: Array <certificate>;
}

export class experence {
    expTitle ?: string;
    country ?: string;
    expYearStart ?: number;
    expYearEnd ?: number;
    expOrganization ?: string;
    expCertificate ?: string;
}

export class education {
    eduTitle ?: string;
    eduYearStart ?: number;
    eduYearEnd ?: number;
    eduOrganization ?: string;
    eduCertificate ?: string;
}

export class certificate {
    cerTitle ?: string;
    cerCertificate ?: string
}

export class signUpPost {
    constructor(
        public email?: string,
        public meta: meta | string = null,
    ){}
}

export class confirmSignUp {
    email ?: string;
    confirm_code ?: string;
    pubkey ?: string;
}

export class meta {
    constructor(
        public region: string,
        public role: string,
        public pass: string,
    ){}
}

export class logIn {
    constructor(
        public email?: string,
        public meta: meta | string = null,
        public pubkey?: string,
    ){}
}

export class allUserData {
    public profile ?: Profile;
    public resume ?: Resume1;
}

export class Profile {
    public tid ?: number;
    public fullname ?: string;
    public email ?: string;
    public avator ?: string;
    public brief_intro ?: string;
    public is_verified ?: true; 
}

export class Resume1 {
    public rid ?: number;
    public tid ?: number;
    public intro ?: string;
    public enable ?: boolean;
    public sections ?: Array<Experience>;
}

export class Experience {
    public sid ?: number;
    public tid ?: number;
    public rid ?: number;
    public order ?: number;
    public subject ?: string;
    public context ?: test1;
}

export class test1 {
    public logo ?: string;
    public desc ?: string;
    public years ?: string;
    public dept ?: string;
    public file_links ?: Array<string>
    public location ?: string;
}

export class resumeSectionGroup {
    public sid ?: number;
    public tid ?: number;
    public rid ?: number;
    public order ?: number;
    public subject ?: string;
    public context ?: test1;
}

export class resumeSectionContext {
public logo ?: string;
public desc ?: string;
public years ?: string;
public dept ?: string;
public file_links = [];
public location ?: string;
}
  


    