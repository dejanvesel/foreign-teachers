import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { allUserData } from '../app.model';
import { AuthService } from '../auth/auth.service';
import { DeleteResumeComponent } from '../shared/delete-resume/delete-resume.component';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private httpClient: HttpClient,
              private _authService : AuthService) { }


  createTeacherResume(obj: allUserData) {
    let header = new HttpHeaders({
      'token': `${this._authService.loggedInToken}`,
      'current-region': 'jp',
    });
    return this.httpClient.post<allUserData>(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/match/teachers/${this._authService.loggedInData.data.auth.role_id}/resumes`, obj, {headers: header}).pipe(map(data => data));
  }  

  updateTeacherResume(obj: allUserData, index: any) {
    let header = new HttpHeaders({
      'current-region': 'jp',
      'token': `${this._authService.loggedInToken}`
    });
    return this.httpClient.put<allUserData>(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/match/teachers/${this._authService.loggedInData.data.auth.role_id}/resumes/${this._authService.loggedInData.data.match.brief_resumes[index].rid}`, obj, {headers: header}).pipe(map(data => data));
  }  

  deleteResume(index: any) {
    let header = new HttpHeaders({
      'current-region': 'jp',
      'token': `${this._authService.loggedInToken}`
    });
    return this.httpClient.delete(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/match/teachers/${this._authService.loggedInData.data.auth.role_id}/resumes/${this._authService.loggedInData.data.match.brief_resumes[index].rid}`, {headers: header}).pipe(map(data => data));
  }

  getResume(index: any) {
    let header = new HttpHeaders({
      'current-region': 'jp',
      'token': `${this._authService.loggedInToken}`
    });
    return this.httpClient.get(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/match/teachers/${this._authService.loggedInData.data.auth.role_id}/resumes/${index}`, {headers: header}).pipe(map(data => data))
  }
}

