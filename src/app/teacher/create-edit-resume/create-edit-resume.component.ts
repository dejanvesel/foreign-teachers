import { TeacherService } from './../teacher.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms'
import { allUserData, Profile, Resume, Resume1, resumeSectionContext, resumeSectionGroup, test1} from 'src/app/app.model';
import { AppService } from 'src/app/app.service';
import { AuthService } from 'src/app/auth/auth.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ChooseResumeComponent } from 'src/app/shared/choose-resume/choose-resume.component';
import { DeleteResumeComponent } from 'src/app/shared/delete-resume/delete-resume.component';
import { delay, Subject, takeUntil } from 'rxjs';
import Swal from 'sweetalert2';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-edit-resume',
  templateUrl: './create-edit-resume.component.html',
  styleUrls: ['./create-edit-resume.component.scss']
})
export class CreateEditResumeComponent implements OnInit, OnDestroy {

  private _unsubscribeAll: Subject<void> = new Subject();

  // PROFILE PICTURE VARIABLES
  imgPreviewSrc: string | ArrayBuffer;
  isImgSelected: boolean = false;
  // ARRAY OF TAGS FOR SELECTION
  tags = [{ id: 1, name: "business english" }, { id: 2, name: "IELTS" }, { id: 3, name: "Daily English" }, { id: 4, name: "Interview" }, { id: 5, name: "TEFL" }, { id: 6, name: "Kids Courses" }, { id: 7, name: "TESOL" }, { id: 8, name: "text" }];
  checkedTags = [];

  showChoose = true;
  show0 = false;

  resumesLength:any = null;

  resumeForm: FormGroup;
  resumeObj: Resume = new Resume;

  allUserData: allUserData = new allUserData;
  test1: test1 = new test1;
  newIndex: number = 0;
  userData: any = null;
  isNewResume: boolean = true;
  requestCompleted: boolean = false;
  deleteResume: any;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private _formBuilder: FormBuilder,
    public _appService : AppService,
    private _teacherService : TeacherService,
    private _authService :  AuthService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,)
  {
    this.initFormGroup();
    this._appService.titleChange.next("My Resume");
    this._appService.defaultHeader = false;

    this._authService.activeUserData.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      data => {
        if (data) {
          this.userData = data;
          this.checkPreviousData();
        }
      }
    )
  }

  initFormGroup() {
  this.resumeForm = this._formBuilder.group({
    profilePicture: ['', Validators.required],
    name: ['', Validators.required],
    email: ['', Validators.required],
    introduction: ['', Validators.required],
    experence: this._formBuilder.array([]),
    education: this._formBuilder.array([]),
    certificate: this._formBuilder.array([]),
    });

  }

  //ADDING ADDITIONAL FIELDS
  addNewExperenceGroup() {
    const exp = this.resumeForm.get('experence') as FormArray;
    exp.push(
      this._formBuilder.group({
        logo: ['', Validators.required],
        expTitle: ['', Validators.required],
        country: ['', Validators.required],
        expYearStart: ['', Validators.required],
        expYearEnd: ['', Validators.required],
        expOrganization: ['', Validators.required],
        expCertificate: ['', Validators.required],
        isSaved:[false],
      })
    );
  }

  addNewEducationGroup() {
    const edu = this.resumeForm.get('education') as FormArray;
    edu.push(
      this._formBuilder.group({
        logo: ['', Validators.required],
        eduTitle: ['', Validators.required],
        eduYearStart: ['', Validators.required],
        eduYearEnd: ['', Validators.required],
        eduOrganization: ['', Validators.required],
        eduCertificate: ['', Validators.required],
        isSaved:[false],
      })
    );
  }
  
  addNewCertificateGroup() {
    const cer = this.resumeForm.get('certificate') as FormArray;
    cer.push(
      this._formBuilder.group({
        cerTitle: ['', Validators.required],
        cerCertificate: ['', Validators.required],
        isSaved:[false],
      })
    );
  }

  //calling functions oninit so form fields appear by default
  ngOnInit(): void {
    this.addNewExperenceGroup();
    this.addNewEducationGroup();
    this.addNewCertificateGroup();
    this.checkPreviousData();
  }

  uploadLogoExperience(event: any, i: number) {
    let selectedFile = (event.target as HTMLInputElement).files?.item(0)

     if (selectedFile) {
      if (["image/jpeg", "image/png", "image/svg+xml"].includes(selectedFile.type)) {
        let fileReader = new FileReader();
        fileReader.readAsDataURL(selectedFile);

        fileReader.addEventListener('load', (event) => {
          const exp = this.resumeForm.get('experence') as FormArray;
          exp.at(i).get('logo').patchValue(event.target?.result);
        })
      }
    }
  }

  uploadLogoEducation(event: any, i:number) {
    let selectedFile = (event.target as HTMLInputElement).files?.item(0)

    if (selectedFile) {
     if (["image/jpeg", "image/png", "image/svg+xml"].includes(selectedFile.type)) {
       let fileReader = new FileReader();
       fileReader.readAsDataURL(selectedFile);

       fileReader.addEventListener('load', (event) => {
         const exp = this.resumeForm.get('education') as FormArray;
         exp.at(i).get('logo').patchValue(event.target?.result);
       })
     }
   }
  }

  // DISPLAY CHOSEN FILE/IMAGE
  showPreview(event: any, i: number) {
    let selectedFile = (event.target as HTMLInputElement).files?.item(0)

    // using just the name
    if (selectedFile.name) {
      const exp = this.resumeForm.get('experence') as FormArray;
      exp.at(i).get('expCertificate').patchValue(selectedFile.name);
    }

  }
  showPreview1(event: Event, i: number) {
    let selectedFile = (event.target as HTMLInputElement).files?.item(0)

    // using just the name
    if (selectedFile.name) {
      const exp = this.resumeForm.get('education') as FormArray;
      exp.at(i).get('eduCertificate').patchValue(selectedFile.name);
    }

  }
  showPreview2(event: Event, i:number) {
    let selectedFile = (event.target as HTMLInputElement).files?.item(0)

    // using just the name
    if (selectedFile.name) {
      const exp = this.resumeForm.get('certificate') as FormArray;
      exp.at(i).get('cerCertificate').patchValue(selectedFile.name);
    }

  }

  showPreviews(event: Event) {
    let selectedFile = (event.target as HTMLInputElement).files?.item(0)

    if (selectedFile) {
      if (["image/jpeg", "image/png", "image/svg+xml"].includes(selectedFile.type)) {
        let fileReader = new FileReader();
        fileReader.readAsDataURL(selectedFile);

        fileReader.addEventListener('load', (event) => {
          this.imgPreviewSrc = event.target?.result;
          this.isImgSelected = true
        })
      }
    }
    else {
      this.isImgSelected = false
    }
  }
  // DELETE FILE/IMAGE
  removeImg(i: number) {
    const exp = this.resumeForm.get('experence') as FormArray;
    exp.at(i).get('expCertificate').patchValue('');
  }
  removeImg1(i: number) {
    const edu = this.resumeForm.get('education') as FormArray;
    edu.at(i).get('eduCertificate').patchValue('');
  }
  removeImg2(i: number) {
    const cer = this.resumeForm.get('certificate') as FormArray;
    cer.at(i).get('cerCertificate').patchValue('');
  }


  // SAVING FORM
  profile = new Profile();
  resume = new Resume1();
  test: number = -1;

  saveForm() {
    let _resume;
    if (this._authService.loggedInData.data.match.brief_resumes[this.newIndex]) {
      this.test = this._authService.loggedInData.data.match.brief_resumes[this.newIndex].rid;
      _resume = this._authService.loggedInData.data.match.brief_resumes[this.newIndex];
    } else {
      this.test = -1;
    }

    // PROFILE
    this.profile.avator = this.resumeForm.get('profilePicture').value;
    this.profile.email = this.resumeForm.get('email').value;
    this.profile.fullname = this.resumeForm.get('name').value;
    this.profile.is_verified = true;
    this.profile.tid = this._authService.loggedInData.data.auth.role_id;
    this.allUserData.profile = this.profile;

    // RESUME
    this.resume.intro = this.resumeForm.get('introduction').value;
    this.resume.enable = true;
    this.resume.tid = !this.isNewResume ? _resume.tid : 0;
    this.resume.rid = !this.isNewResume ? _resume.rid : 0;
    this.resume.sections = [];
    this.allUserData.resume = this.resume;

    let active_index = 0;

    // SECTIONS
    // -- Experience
    const _exp = this.resumeForm.get('experence') as FormArray;
    _exp.controls.forEach((el, index) => {
      let exp: resumeSectionGroup = new resumeSectionGroup();
      if (!this.isNewResume && (_resume && _resume.sections && _resume.sections[active_index])) {
        exp.order = _resume.sections[active_index].order;
        exp.rid = _resume.sections[active_index].rid;
        exp.sid = _resume.sections[active_index].sid;
        exp.tid = _resume.sections[active_index].tid;
        exp.subject = _resume.sections[active_index].subject;
        active_index++;
      } else
      if (el.value.isSaved) {
        exp.order = 0;
        exp.rid = 0;
        exp.sid = 0;
        exp.tid = 0;
        exp.subject = 'Experience';
      }

      let context: resumeSectionContext = new resumeSectionContext();
      context.dept = el.value.expTitle;
      context.desc = el.value.expOrganization;
      context.location = el.value.country;
      context.years = el.value.expYearStart + '-' + el.value.expYearEnd;
      context.file_links.push(el.value.expCertificate);
      context.logo = el.value.logo;

      exp.context = context;

      this.allUserData.resume.sections.push(exp);
    })

    // -- Education
    const _edu = this.resumeForm.get('education') as FormArray;
    _edu.controls.forEach((el, index) => {
      let edu: resumeSectionGroup = new resumeSectionGroup();
      if (!this.isNewResume && (_resume && _resume.sections && _resume.sections[active_index])) {
        edu.order = _resume.sections[active_index].order;
        edu.rid = _resume.sections[active_index].rid;
        edu.sid = _resume.sections[active_index].sid;
        edu.tid = _resume.sections[active_index].tid;
        edu.subject = _resume.sections[active_index].subject;
        active_index++;
      }else
      if (el.value.isSaved) {
        edu.order = 0;
        edu.rid = 0;
        edu.sid = 0;
        edu.tid = 0;
        edu.subject = 'Education';
      }

      let context: resumeSectionContext = new resumeSectionContext();
      context.dept = el.value.eduTitle;
      context.desc = el.value.eduOrganization;
      context.years = el.value.eduYearStart + '-' + el.value.eduYearEnd;
      context.file_links.push(el.value.eduCertificate);
      context.logo = el.value.logo;

      edu.context = context;

      this.allUserData.resume.sections.push(edu);
    })

    // -- Certificates
    const _cer = this.resumeForm.get('certificate') as FormArray;
    _cer.controls.forEach((el, index) => {
      let cer: resumeSectionGroup = new resumeSectionGroup();
      if (!this.isNewResume && (_resume && _resume.sections && _resume.sections[active_index])) {
        cer.order = _resume.sections[active_index].order;
        cer.rid = _resume.sections[active_index].rid;
        cer.sid = _resume.sections[active_index].sid;
        cer.tid = _resume.sections[active_index].tid;
        cer.subject = _resume.sections[active_index].subject;
        active_index++;
      }
      if (el.value.isSaved) {
        cer.order = 0;
        cer.rid = 0;
        cer.sid = 0;
        cer.tid = 0;
        cer.subject = 'Certificates';
      }

      let context: resumeSectionContext = new resumeSectionContext();
      context.dept = el.value.cerTitle;
      context.file_links.push(el.value.cerCertificate);

      cer.context = context;

      this.allUserData.resume.sections.push(cer);
    });

  }

  saveResume() {
    this.requestCompleted = false;

    if(!this.isNewResume) {
      this.allUserData.resume.sections.forEach(el => {
        this.allUserData.resume.rid = this._authService.loggedInData.data.match.brief_resumes[this.newIndex].rid;
        el.rid = this._authService.loggedInData.data.match.brief_resumes[this.newIndex].rid;
      });

      this._teacherService.updateTeacherResume(this.allUserData, this.newIndex).subscribe(
        data => {
          this._snackBar.open(`Resume successfully saved!`, 'OK', {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          }).afterDismissed().pipe(delay(0)).subscribe(
            () => {
              this.requestCompleted = true;
              this.refreshActiveUser();
              setTimeout(() => {
                this.showAllResumes();
                this.imgPreviewSrc = null;
                this.isImgSelected = false;
                this.allUserData = new allUserData();
              }, 1000)
            }
          );
        }, error => {
          this._snackBar.open(`Oops! Something went wrong!`, 'OK', {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          }).afterDismissed().pipe(delay(0)).subscribe(
            () => {
              this.requestCompleted = true;
            }
          );
        }
      )
    }
    else {
      this._teacherService.createTeacherResume(this.allUserData).subscribe(
        data => {
          this._snackBar.open(`Resume successfully saved!`, 'OK', {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          }).afterDismissed().pipe(delay(0)).subscribe(
            () => {
              this.requestCompleted = true;
              this.refreshActiveUser();
              setTimeout(() => {
                this.showAllResumes();
                this.imgPreviewSrc = null;
                this.isImgSelected = false;
                this.allUserData = new allUserData();
              }, 1000)
            }
          );
        }, error => {
          this._snackBar.open(`Oops! Something went wrong!`, 'OK', {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          }).afterDismissed().pipe(delay(0)).subscribe(
            () => {
              this.requestCompleted = true;
            }
          );
        }
      )
    }
  }



  // CHECK IF THERE ARE SAVED RESUMES FOR THE LOGGED USER
  checkPreviousData() {
    this.resumesLength = this._authService.loggedInData;
    // Patch profile existing data
    if (this.resumesLength && this.resumesLength.data.match.brief_resumes.length > 0 ) {
      this.patchProfileData();
    }

    this.resumesLength = (this.resumesLength && this.resumesLength.data) ? this.resumesLength.data.match.brief_resumes : [];
    if(this.resumesLength.length > 0) {
      this.showChoose = false,
      this.show0 =  true;

      // Patch form groups
      this.patchFormArray();
    }
  }

  patchProfileData() {
    this.resumeForm.patchValue ({
      //profilePicture: this._authService.loggedInData.data.auth. // Picture is not returned from API
      //name: this._authService.loggedInData.data.auth.  // Name is not returned from API
      email: this._authService.loggedInData.data.auth.email,
      introduction: this._authService.loggedInData.data.match.brief_resumes[this.newIndex].intro,
    });

    this.resumeObj.name = this._authService.loggedInData.data.auth.email;
    this.resumeObj.email = this._authService.loggedInData.data.match.brief_resumes[this.newIndex].intro;
  }

  patchFormArray() {
    if (this.resumesLength[this.newIndex]) {
      if (this.resumesLength[this.newIndex].sections) {

        let defExp: boolean = true;
        let defEdu: boolean = true;
        let defCer: boolean = true;

        this.resumesLength[this.newIndex].sections.forEach((section, i) => {

          // Check if it's Experience
          if (section.subject == 'Experience') {
              const exp = this.resumeForm.get('experence') as FormArray;
              exp.push(
                this._formBuilder.group({
                  logo: [section.context.logo],
                  expTitle: [section.context.dept],
                  country: [section.context.location ? section.context.location : null],
                  expYearStart: [section.context.years ? section.context.years.slice(0, 4) : null],
                  expYearEnd: [section.context.years ? section.context.years.slice(-4) : null],
                  expOrganization: [section.context.desc],
                  expCertificate: [section.context.file_links ? section.context.file_links[0] : null],
                  isSaved:[false],
                })
              );

              if (defExp) {
                exp.removeAt(0); // Remove the default Experience empty group
                defExp = false;
              }


          };

          // Check if it's Education
          if (section.subject == 'Education') {
            const edu = this.resumeForm.get('education') as FormArray;
            edu.push(
              this._formBuilder.group({
                logo: [section.context.logo],
                eduTitle: [section.context.dept],
                eduYearStart: [section.context.years ? section.context.years.slice(0, 4) : null],
                eduYearEnd: [section.context.years ? section.context.years.slice(-4) : null],
                eduOrganization: [section.context.desc],
                eduCertificate: [section.context.file_links ? section.context.file_links[0] : null],
                isSaved:[false],
              })
            );

            if (defEdu) {
              edu.removeAt(0); // Remove the default Education empty group
              defEdu = true;
            }
          };

          // Check if it's Education
          if (section.subject == 'Certificates') {
            const cer = this.resumeForm.get('certificate') as FormArray;
            cer.push(
              this._formBuilder.group({
                cerTitle: [section.context.dept],
                cerCertificate: [section.context.file_links ? section.context.file_links[0] : null],
                isSaved:[false],
              })
            );

            if (defCer) {
              cer.removeAt(0); // Remove the default Certificates empty group
              defCer = true;
            }

          };

        })
      }
    }


  }

  // SHOW AND HIDE PAGES ON CLICK FUNCTIONS
  showResume() {
    this._appService.isShow = true;
    this._appService.isShowForm = true;
    this._appService.isShowResume = false;
  }
  showForm() {
    this._appService.isShow = true;
    this._appService.isShowForm = false;
    this._appService.isShowResume = true;
  }
  showAllResumes(){
    if (this.resumeForm.dirty && !this.requestCompleted) {
      Swal.fire({
        title: 'You have unsaved changes! Are you sure?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Yes',
        denyButtonText: `Cancel`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          this._appService.isShow = false;
          this._appService.isShowForm = true;
          this._appService.isShowResume = true;
          this.imgPreviewSrc = null;
          this.isImgSelected = false;
          this.allUserData = new allUserData();
        }
      })
    } else {
      this._appService.isShow = false;
      this._appService.isShowForm = true;
      this._appService.isShowResume = true;
    }

  }

  // SAVING SELECTED TAGS
  onCheck(event) {
    if (!this.checkedTags.includes(event)) {
      this.checkedTags.push(event);
    } else {
      let index = this.checkedTags.indexOf(event);
      if (index > -1) {
        this.checkedTags.splice(index, 1);
      }
    }
    // console.log(this.checkedTags)

  }

  /**
  * Exprience functions
  */

  // Add new experience group
  addExperencePreview(index:number) {
    const exp = this.resumeForm.get('experence') as FormArray;
    exp.at(index).get('isSaved').setValue(true);
  }

  // Clear form array group
  clearExperencePreview(index: number) {
    const exp = this.resumeForm.get('experence') as FormArray;
    const expVal = exp.at(index);
    expVal.get("expTitle").setValue('');
    expVal.get("country").setValue('');
    expVal.get("expYearStart").setValue('');
    expVal.get("expYearEnd").setValue('');
    expVal.get("expOrganization").setValue('');
    expVal.get("expCertificate").setValue('');
  }

  // Delete exprience group
  deleteExperencePreview(index: number) {
   const exp = this.resumeForm.get('experence') as FormArray;
   exp.removeAt(index);
  }


  /**
  * Education functions
  */

  // Add new education group
  addEducationPreview(index: number) {
    const exp = this.resumeForm.get('education') as FormArray;
    exp.at(index).get('isSaved').setValue(true);
  }

  // Delete education group
  deleteEducationPreview(index: number) {
    const exp = this.resumeForm.get('education') as FormArray;
    exp.removeAt(index);
  }


  /**
  * Certificate functions
  */

  // Add new certificate group
  addCertificatePreview(index: number) {
    const exp = this.resumeForm.get('certificate') as FormArray;
    exp.at(index).get('isSaved').setValue(true);
  }

  // Delete certificate group
  deleteCertificatePreview(index: number) {
    const exp = this.resumeForm.get('certificate') as FormArray;
    exp.removeAt(index);
  }

  openDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    // dialogConfig.data = {
    //   id: 1,
    //   title: 'Angular For Beginners'
    // };

    let dialogWidth: string;
    let dialogHeight: string;
    let positionBottom: string;

    if (window.innerWidth < 768) {
      dialogWidth = '100%';
      dialogHeight = '100%';
      positionBottom = '0px';

    } else {
      dialogWidth = '780px';
      dialogHeight = '100%';
      positionBottom = '';
    }

    const dialogRef = this.dialog.open(ChooseResumeComponent, {
      height: dialogWidth,
      width: dialogHeight,
      maxWidth: '600px',
      maxHeight: '50vh',
      panelClass: 'custom-modalbox',
      position: {
        bottom: positionBottom,
      }

    });
    dialogRef.afterClosed().subscribe((response) => {
      if(response) {
          this.newIndex = JSON.parse(response);
          this.resumeForm.reset();
          this.isNewResume = false;
          this.checkPreviousData();
          this.showForm();
      }
    });

  }
  
  openDialogDelete() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    let dialogWidth: string;
    let dialogHeight: string;
    let positionBottom: string;

    if (window.innerWidth < 768) {
      dialogWidth = '100%';
      dialogHeight = '100%';
      positionBottom = '0px';

    } else {
      dialogWidth = '780px';
      dialogHeight = '100%';
      positionBottom = '';
    }

    const dialogRef = this.dialog.open(DeleteResumeComponent, {
      height: dialogWidth,
      width: dialogHeight,
      maxWidth: '600px',
      maxHeight: '50vh',
      panelClass: 'custom-modalbox',
      position: {
        bottom: positionBottom,
      }

    });

    dialogRef.afterClosed().subscribe((response) => {
      if(response) {
      
          this._teacherService.deleteResume(this.newIndex).subscribe(
            data => {
              this._snackBar.open(`Resume successfully deleted!`, 'OK', {
                duration: 2000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              }).afterDismissed().pipe(delay(0)).subscribe(
                () => {
                  this.requestCompleted = true;
                  this.refreshActiveUser();
                  this._appService.isShow = false;
                  this._appService.isShowForm = true;
                  this._appService.isShowResume = true;
                  this.imgPreviewSrc = null;
                  this.isImgSelected = false;
                  setTimeout(() => {
                    this.allUserData = new allUserData();
                  }, 1000)
                }
              );
          })
        }
      })
  }

  refreshForm() {
    this.resumeObj = new Resume();
    this.imgPreviewSrc = null;

    this.resumeForm.reset();

    this.initFormGroup();

    this.addNewExperenceGroup();
    this.addNewEducationGroup();
    this.addNewCertificateGroup();

  }


  refreshActiveUser() {
    let token = this._authService.getToken();
    if (token) {
      let obj = JSON.parse(token);
      this._authService.logIn(obj).subscribe(data => {
        this._authService.loggedInData = data;
        this._authService.loggedInToken = this._authService.loggedInData.data.auth.token;
        this._authService.activeUserData.next(data);
      }, error => {
      });
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
