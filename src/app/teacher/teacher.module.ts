import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeacherRoutingModule } from './teacher-routing.module';
import { FindJobComponent } from './find-job/find-job.component';
import { TeachersCardsComponent } from './teachers-cards/teachers-cards.component';
import { CreateEditResumeComponent } from './create-edit-resume/create-edit-resume.component';
import { PositionsComponent } from './positions/positions.component';
import { SharedModule } from '../shared/shared.module';
import { TeachersDetailComponent } from './teachers-detail/teachers-detail.component';
import { TeachersListComponent } from './teachers-list/teachers-list.component';




@NgModule({
  declarations: [
    FindJobComponent,
    TeachersCardsComponent,
    CreateEditResumeComponent,
    PositionsComponent,
    TeachersDetailComponent,
    TeachersListComponent
  ],
  imports: [
    CommonModule,
    TeacherRoutingModule,
    SharedModule
  ]
})
export class TeacherModule { }
