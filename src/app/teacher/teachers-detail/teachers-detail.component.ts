import { Component, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { CompanyService } from 'src/app/company/company.service';
import { TeacherService } from '../teacher.service';

@Component({
  selector: 'app-teachers-detail',
  templateUrl: './teachers-detail.component.html',
  styleUrls: ['./teachers-detail.component.scss']
})
export class TeachersDetailComponent implements OnInit {

  private _unsubscribeAll: Subject<void> = new Subject();
  public index: any;
  public resumeDetails: any;
  
  constructor(private _authService: AuthService, private _companyService: CompanyService, private _teacherService: TeacherService) { }

  ngOnInit(): void {
    this._authService.activeUserData.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      data => {
        if (data) {
          if (this.index) {
            if (!this.resumeDetails) {
              this.getResume();
            }
          }
            
        }
      })
    this._companyService.jobDetailsIndex.subscribe( 
      data => {
        this.index = data;
        this.getResume();
      }
    )
  }

  getResume() {
    this._teacherService.getResume(this.index).subscribe(
      data => {
        this.resumeDetails = data;
        this.resumeDetails = this.resumeDetails.data;
    })
  }

  readMore() {
    let dots = document.getElementById("dots");
    let moreText = document.getElementById("more");
    let btnText = document.getElementById("myBtn");
  
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "read more";
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "read less";
      moreText.style.display = "inline";
    }
  }

}
