import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateEditResumeComponent } from './create-edit-resume/create-edit-resume.component';
import { FindJobComponent } from './find-job/find-job.component';
import { PositionsComponent } from './positions/positions.component';
import { TeachersCardsComponent } from './teachers-cards/teachers-cards.component';
import { TeachersDetailComponent } from './teachers-detail/teachers-detail.component';
import { TeachersListComponent } from './teachers-list/teachers-list.component';
const routes: Routes = [
  {
    path: 'create-edit-resume',
    component: CreateEditResumeComponent
  },
  {
    path: 'find-job',
    component: FindJobComponent
  },
  {
    path: 'positions',
    component: PositionsComponent
    },
  {
    path: 'teachers-cards',
    component: TeachersCardsComponent
  },
  {
    path: 'teachers-detail',
    component: TeachersDetailComponent
  },
  {
    path: 'teachers-list',
    component: TeachersListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeacherRoutingModule { }
