import { Component, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { CompanyService } from 'src/app/company/company.service';

@Component({
  selector: 'app-teachers-list',
  templateUrl: './teachers-list.component.html',
  styleUrls: ['./teachers-list.component.scss']
})
export class TeachersListComponent implements OnInit {

  constructor(private _authService: AuthService, private _companyService: CompanyService) {}

  public show1: boolean = false;
  public resumes : any;
  private _unsubscribeAll: Subject<void> = new Subject();
  public id: number;


  ngOnInit(): void {
    this._authService.activeUserData.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      data => {
        if (data) {
            this.resumes = data;
            this.resumes = this.resumes.data.match.brief_resumes;
            console.log("resumes", this.resumes)
        }})
  }
  readMore() {
    let dots = document.getElementById("dots");
    let moreText = document.getElementById("more");
    let btnText = document.getElementById("myBtn");
  
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "read more";
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "read less";
      moreText.style.display = "inline";
    }
  }
   //reloads current page 
   refresh(): void {
    window.location.reload();
}

  showDetails(resumes, index) {
    this.id = resumes[index].rid;
    this._companyService.jobDetailsIndex.next(this.id);
  }
}
