import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AppService } from 'src/app/app.service';
import { DeleteApplicationComponent } from 'src/app/shared/delete-application/delete-application.component';
@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss']
})
export class ApplicationsComponent implements OnInit {
  private dialogRef: any;
  //private _matDialog:any ;

  constructor(public _appService: AppService, private _matDialog: MatDialog) {
    this._appService.titleChange.next("My Aplications");
    this._appService.defaultHeader = false;
  }
  
  ngOnInit(): void {
  }
  openDeleteDialog() { 
     this.dialogRef = this._matDialog.open(DeleteApplicationComponent, {
     // shirina za dialog  
    //  width: '600px'
    }); 
    this.dialogRef.afterClosed().subscribe((response) => { 
      if(response) { 
        
      }
    })

  }
}
