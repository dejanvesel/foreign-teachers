import { AppService } from 'src/app/app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MyPostsComponent } from './../../account-pages/my-posts/my-posts.component';
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';
import { NguCarouselConfig } from '@ngu/carousel';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { companyData, Job, Job_desc, newPost, Others, Overview, Photos, Profile, Sections } from './create.model';
import { AppComponent } from 'src/app/app.component';
import { AuthService } from 'src/app/auth/auth.service';
import { delay, Subject, takeUntil } from 'rxjs';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DeletePostComponent } from 'src/app/shared/delete-post/delete-post.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-edit-post-job',
  templateUrl: './create-edit-post-job.component.html',
  styleUrls: ['./create-edit-post-job.component.scss']
})
export class CreateEditPostJobComponent implements OnInit, AfterViewChecked {

  private _unsubscribeAll: Subject<void> = new Subject();
  public createAPost1: boolean = false;
  public showPreview: boolean = false;
  public continue: boolean = false;
  public change: boolean = false;
  public finished: boolean = false;
  public myPostsView: boolean = false;
  public editPostsBtns: boolean = false;
  public imagePreviewSrc: string | ArrayBuffer | null | undefined = '';
  public uploadedImgs: any = [];
  public createPost: FormGroup;
  public createNewPost= new newPost();
  public section1: Sections = new Sections;
  public section2: Sections = new Sections;
  public section3: Sections = new Sections;
  public section4: Sections = new Sections;
  public companyData: companyData = new companyData;
  public profile: Profile = new Profile;
  public overview: Overview = new Overview;
  public resume: Job = new Job;
  public job_desc: Job_desc = new Job_desc;
  public jobData: any;
  public job_id: any;
  public j_id: any;
  public jobsLength:any = null;
  public getJob: any;
  public requestCompleted: boolean = false;
  public isNewResume: boolean = true;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';


  constructor(
    private _cdRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _appComponent: AppComponent,
    private _router: Router,
    public _appService: AppService,
    public _companyService: CompanyService,
    private _authService: AuthService,
    private _route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private dialog: MatDialog,
    private router: Router
    ) {
      this._appService.titleChange.next("Post Job");
      this._appService.defaultHeader = true;

      this.createPost = this._formBuilder.group({
        title: new FormControl('', Validators.required),
        country: new FormControl('', Validators.required),
        salary_low: new FormControl('', Validators.required),
        salary_high: new FormControl('', Validators.required),
        company_size: new FormControl('', Validators.required),
        company_founded: new FormControl('', Validators.required),
        company_revenue: new FormControl('', Validators.required),
        company_url: new FormControl('', Validators.required),
        company_jobTitle: new FormControl('', Validators.required),
        company_description: new FormControl('', Validators.required),
        text_who: new FormControl('', Validators.required),
        text_what: new FormControl('', Validators.required),
        text_values: new FormControl('', Validators.required),
        text_where: new FormControl('', Validators.required),
      })
   }
  ngAfterViewChecked(): void {
    this._cdRef.detectChanges();
  }

  showForm() {
    this._appService.createAPost = true;
  }

  public carouselTileItems: Array<any> = [0, 1, 2, 3, 4, 5];
  public carouselTiles = {
    0: [],
    1: [],
    2: [],
    3: [],
    4: [],
    5: []
  };
  public carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 1, md: 1, lg: 1, xl: 1, all: 0 },
    slide: 3,
    speed: 250,
    point: {
      visible: true
    },
    load: 2,
    velocity: 0,
    touch: true,
    easing: 'cubic-bezier(0, 0, 0.2, 1)'
  };

  ngOnInit(): void {
    this._authService.activeUserData.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      data => {
        if (data) {
          // Sega tuka veke go imame 'j_id' (linija 143)
          // Isto taka tuka gi dobivame i podatocite od Login apito, taka da sega veke mozeme da go povikame apito za Post Job Details

          // Pravime za sekoj sluacj proverka dali postoi sigurno 'jid' vo slucaj da ne sme prenesle id vo url
          if (this.j_id) {
            // Cekor 3. - Go vikame apito za Post Job Details
            if (!this.getJob) {
              this.getPostJobDetails();
            }
          }
        }
      }
    )

    if(this._appComponent.showTheForm) {
      this._appService.createAPost = true;
    }
    if(this._appComponent.switchUp){
      this.showPreview = true;
      this.myPostsView = true;
      this.change = true;
    }

    this.carouselTileItems.forEach(el => {
      this.carouselTileLoad(el);
    });

    // Query parametri gi zemame so 'queryParams' namesto so 'queryParamMap'
    this._route.queryParams.subscribe(params => {
      if (params['id']) {
        // Cekor 1. - Proveruvas dali vo url ima jid
        this.j_id = params['jid'];
        this.job_id = params['id'];
        this.isNewResume = false;

        if (!this.getJob) {
          this.getPostJobDetails();
        }
        // Cekor 2. - Cekame Login apito da vrati podatoci (zaradi role_id), da se zacuvaat podatocite za userot, pa duri togas da go povikas apito za Post Job Details
        // Prodolzuva kaj subscribe-ot na 'activeUserData' ...
      }
    });
  }

  getPostJobDetails() {
    this._companyService.getJob(this.j_id).subscribe(
      data => {
        this.getJob = data;
        console.log("Get Job Details",this.getJob);
        // Cekoj 4. - Otkako gi dobivme detalite za Post Job, pravime Patch Values
        // Funckijata 'checkPreviousData' ne ti ni treba, oti tie podatoci gi koristis samo koga gi prikazuvas na 'My Posts'
        this.patchData();
    })
  }

  public carouselTileLoad(j) {
    const len = this.carouselTiles[j].length;
    if (len <= 30) {
      for (let i = len; i < len + 15; i++) {
        this.carouselTiles[j].push(
          this.uploadedImgs[Math.floor(Math.random() * this.uploadedImgs.length)],
        );
      }
    }
  }

  displayAddedImages(event) {
    let selectedFile = (event.target as HTMLInputElement).files?.item(0);

    if (["image/jpeg", "image/png", "image/svg+xml"].includes(selectedFile.type)) {
      let fileReader = new FileReader();
      fileReader.readAsDataURL(selectedFile);

      fileReader.addEventListener('load', (event) => {
        this.imagePreviewSrc = event.target?.result;
        this.uploadedImgs.push(this.imagePreviewSrc);
      })

      this.carouselTileItems.forEach(el => {
        this.carouselTileLoad(el);
      });
    }
  }

  test: number = -1;
  _resume: any;
  goToPreview() {
    this.requestCompleted = false;
    let salary: string;
    this.profile.sections = [];
    if (this._authService.loggedInData.data.match.brief_jobs[this.job_id]) {
      this.test = this._authService.loggedInData.data.match.brief_jobs[this.job_id].jid;
      this._resume = this._authService.loggedInData.data.match.brief_jobs[this.job_id];
      this.test = -1;
    }

    if(this.createPost.valid) {
      this.createNewPost.title = this.createPost.get('title').value;
      this.createNewPost.country = this.createPost.get('country').value;
      this.createNewPost.salary_low = this.createPost.get('salary_low').value;
      this.createNewPost.salary_high = this.createPost.get('salary_high').value;
      this.createNewPost.company_size = this.createPost.get('company_size').value;
      this.createNewPost.company_founded = this.createPost.get('company_founded').value;
      this.createNewPost.company_revenue = this.createPost.get('company_revenue').value;
      this.createNewPost.company_url = this.createPost.get('company_url').value;
      this.createNewPost.company_jobTitle = this.createPost.get('company_jobTitle').value;
      this.createNewPost.company_description = this.createPost.get('company_description').value;
      this.createNewPost.text_who = this.createPost.get('text_who').value;
      this.createNewPost.text_what = this.createPost.get('text_what').value;
      this.createNewPost.text_values = this.createPost.get('text_values').value;
      this.createNewPost.uploadedImgs = this.uploadedImgs;
      this.createNewPost.text_where = this.createPost.get('text_where').value;

      //add api call
      this.continue = true;
      this._appService.createAPost = false;
      this.showPreview = true;
    }

    salary = `${this.createPost.get('salary_low').value} - ${this.createPost.get('salary_high').value} uSD / Month`;
    this.profile.cid = !this.isNewResume ? this._resume.jid : 0;
    this.profile.intro = "";
    this.profile.logo = "";
    this.profile.name = this.createPost.get('title').value;

    this.overview.Founded = this.createPost.get('company_founded').value;
    this.overview.Revenue = this.createPost.get('company_revenue').value;
    this.overview.Size = this.createPost.get('company_size').value;
    this.overview.URL = this.createPost.get('company_url').value;
    this.uploadedImgs.forEach((el, i) => {
      let photo = new Photos();
      photo.filename = "Image " + i;
      photo.link = el;
      this.profile.photos.push(photo)
    })

    this.section1.title = "Who We Are";
    this.section1.desc = this.createPost.get('text_who').value;
    this.profile.sections.push(this.section1);

    this.section2.title = "What We Do";
    this.section2.desc = this.createPost.get('text_what').value;
    this.profile.sections.push(this.section2);

    this.section3.title = "Cultures & Values";
    this.section3.desc = this.createPost.get('text_who').value;
    this.profile.sections.push(this.section3);

    this.section4.title = "Where we are";
    this.section4.desc = this.createPost.get('text_where').value;
    this.profile.sections.push(this.section4);

    this.resume.cid = !this.isNewResume ? this._resume.jid : 0;
    this.resume.enable = true;
    this.resume.jid = !this.isNewResume ? this._resume.jid : 0;
    this.resume.region = this.createPost.get('country').value;
    this.resume.salary = salary;
    this.resume.title = this.createPost.get('company_jobTitle').value;
    this.job_desc.desc = this.createPost.get('company_description').value;
    this.resume.others = {};

    this.profile.overview = this.overview;
    this.resume.job_desc = this.job_desc;
    this.companyData.job = this.resume;
    this.companyData.profile = this.profile;
  }

  patchData() {

    // Format Salary
    let _salary = this.getJob.data.job.salary;
    let _splitSalary: string;
    let _firstAmount: string;
    let _secondAmout: string;

    if (_salary && _salary != '') {
      _splitSalary = this.getJob.data.job.salary.split(' - ');
      _firstAmount = _splitSalary[0];
      _secondAmout = _splitSalary[1];
      _secondAmout = _secondAmout.toLocaleLowerCase();
      if (_secondAmout.includes('usd')) {
        _secondAmout = _secondAmout.split(' usd')[0];
      }
    }

    if (_firstAmount && _secondAmout) {
      this.createNewPost.salary_low = _firstAmount;
      this.createNewPost.salary_high = _secondAmout;
    }

    // Path values to form
    this.createPost.patchValue ({
      title: this.getJob.data.profile.name,
      country: this.getJob.data.job.region,
      company_size: this.getJob.data.profile.overview.Size,
      company_founded: this.getJob.data.profile.overview.Founded,
      company_revenue: this.getJob.data.profile.overview.Revenue,
      company_url: this.getJob.data.profile.overview.URL,
      company_jobTitle: this.getJob.data.job.title,
      company_description: this.getJob.data.job.job_desc.desc,
      text_who: this.getJob.data.profile.sections[0] ? this.getJob.data.profile.sections[0].desc : null,
      text_what: this.getJob.data.profile.sections[1] ? this.getJob.data.profile.sections[1].desc : null,
      text_values: this.getJob.data.profile.sections[2] ? this.getJob.data.profile.sections[2].desc : null,
      uploadedImgs: this.getJob.data.profile.photos,
      text_where: this.getJob.data.profile.sections[3] ? this.getJob.data.profile.sections[3].desc : null,
      salary_low: _firstAmount,
      salary_high: _secondAmout,
    });

    this.createNewPost.title = this.getJob.data.profile.name;
    this.createNewPost.country = this.getJob.data.job.region;
    this.createNewPost.company_size = this.getJob.data.profile.overview.Size;
    this.createNewPost.company_founded = this.getJob.data.profile.overview.Founded;
    this.createNewPost.company_revenue = this.getJob.data.profile.overview.Revenue;
    this.createNewPost.company_url = this.getJob.data.profile.overview.URL;
    this.createNewPost.company_jobTitle = this.getJob.data.job.title;
    this.createNewPost.company_description = this.getJob.data.job.job_desc.desc;
    this.createNewPost.text_who = this.getJob.data.profile.sections[0].desc;
    this.createNewPost.text_what = this.getJob.data.profile.sections[1].desc;
    this.createNewPost.text_values = this.getJob.data.profile.sections[2].desc;
    this.createNewPost.uploadedImgs = this.getJob.data.profile.photos;
    this.createNewPost.text_where = this.getJob.data.profile.sections[3].desc;
  }

  saveForm() {
    this.requestCompleted = false;
      if (!this.isNewResume) {
        this._companyService.updateJob(this.companyData, this.j_id).subscribe(
          data => {
            this.change = true;
            this.finished = true;
            this._snackBar.open(`Job successfully updated!`, 'OK', {
              duration: 2000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            }).afterDismissed().pipe(delay(0)).subscribe(
              () => {
                this.requestCompleted = true;
                this.refreshActiveUser();
                setTimeout(() => {
                  this.companyData = new companyData();
                }, 1000)
              }
            );
          },
          error => {
            this._snackBar.open(`Oops! Something went wrong!`, 'OK', {
              duration: 2000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            }).afterDismissed().pipe(delay(0)).subscribe(
              () => {
                this.requestCompleted = true;
              }
            );
          })
      }
      else {
        this._companyService.createJob(this.companyData).subscribe(
          data => {
            this.change = true;
            this.finished = true;
            this.router.navigate(['/company/my-posts']);
            this._snackBar.open(`Job successfully saved!`, 'OK', {
              duration: 2000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            }).afterDismissed().pipe(delay(0)).subscribe(
              () => {
                this.requestCompleted = true;
                this.refreshActiveUser();
                setTimeout(() => {
                  this.companyData = new companyData();
                }, 1000)
              }
            );
          },
          error => {
            this._snackBar.open(`Oops! Something went wrong!`, 'OK', {
              duration: 2000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            }).afterDismissed().pipe(delay(0)).subscribe(
              () => {
                this.requestCompleted = true;
              }
            );
          });
      }
  }

  editForm() {
    this._appService.createAPost = true;
    this.showPreview = false;
  }

  editExistingForm() {
    this._appService.createAPost = true;
    this.showPreview = false;
    this.editPostsBtns = true;
  }

  deletePost() {
    this.createNewPost = new newPost;
    this._router.navigate(['./../company/my-posts']);
    this._appComponent.switchUp = false;

    this._companyService.deleteJob(this.j_id).subscribe(
      data => {
        this._snackBar.open(`Job successfully deleted!`, 'OK', {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        }).afterDismissed().pipe(delay(0)).subscribe(
          () => {
            this.requestCompleted = true;
            this.refreshActiveUser();
            setTimeout(() => {
              this.companyData = new companyData();
            }, 1000)
          }
        );
    })
  }

  readMore() {
    let text = document.getElementById("text-who");
    let less = document.getElementById("read-more");
    if(less.innerText === 'Read more'){
      less.innerText = 'Read less';
      text.classList.add("text-show");
    } else {
      less.innerText = 'Read more';
      text.classList.remove("text-show")
    }
  }

  showAllResumes(){
    if (this.createPost.dirty && !this.requestCompleted) {
      Swal.fire({
        title: 'You have unsaved changes! Are you sure?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Yes',
        denyButtonText: `Cancel`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          this._router.navigate(['company/my-posts'])
          this.companyData = new companyData();
        }
      })
    } else {
      this._router.navigate(['company/my-posts'])
    }
  }

  openDialogDelete() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    let dialogWidth: string;
    let dialogHeight: string;
    let positionBottom: string;

    if (window.innerWidth < 768) {
      dialogWidth = '100%';
      dialogHeight = '100%';
      positionBottom = '0px';

    } else {
      dialogWidth = '780px';
      dialogHeight = '100%';
      positionBottom = '';
    }

    const dialogRef = this.dialog.open(DeletePostComponent, {
      height: dialogWidth,
      width: dialogHeight,
      maxWidth: '600px',
      maxHeight: '50vh',
      panelClass: 'custom-modalbox',
      position: {
        bottom: positionBottom,
      }

    });

    dialogRef.afterClosed().subscribe((response) => {
      if(response) {
        this.deletePost();
        }
      })
  }

  refreshActiveUser() {
    let token = this._authService.getToken();
    if (token) {
      let obj = JSON.parse(token);
      this._authService.logIn(obj).subscribe(data => {
        this._authService.loggedInData = data;
        this._authService.loggedInToken = this._authService.loggedInData.data.auth.token;
        this._authService.activeUserData.next(data);
      }, error => {
      });
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
