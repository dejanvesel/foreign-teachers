export class newPost {
    title ?: string;
    country ?: string;
    salary_low ?: string;
    salary_high ?: string;
    company_size ?: string;
    company_founded ?: string;
    company_revenue ?: string;
    company_url ?: string;
    company_jobTitle ?: string;
    company_description ?: string;
    text_who ?: string;
    text_what ?: string;
    text_values ?: string;
    uploadedImgs ?: string;
    text_where ?: string;
}

export class companyData {
    public profile ?: Profile;
    public job ?: Job;
}

export class Profile {
    public cid ?: string;
    public name ?: string;
    public logo ?: string;
    public intro ?: string;
    public overview ?: Overview;
    public sections ?: Array<Sections>;
    public photos : Array<Photos> = new Array();
}

export class Overview {
    public Size ?: string;
    public Founded ?: string;
    public Revenue ?: string;
    public URL ?: string;
}

export class Sections {
    public title ?: string;
    public desc ?: string;
}

export class Photos {
  public filename ?:string;
  public link ?: string;
}

export class Job {
    public jid ?: number;
    public cid ?: number
    public title ?: string;
    public region ?: string;
    public salary ?: string;
    public job_desc ?: Job_desc;
    public others ?: Others;
    public enable ?: boolean;
}

export class Job_desc {
    public desc ?: string;
}

export class Others {

}
