import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApplicationsComponent } from './applications/applications.component';

import { CreateEditPostJobComponent } from './create-edit-post-job/create-edit-post-job.component';
import { FindTeacherComponent } from './find-teacher/find-teacher.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import { JobsListComponent } from './jobs-list/jobs-list.component';
import { MyPostsComponent } from './my-posts/my-posts.component';
import { TeacherFollowsContactComponent } from './teacher-follows-contact/teacher-follows-contact.component';

const routes: Routes = [
  {
    path: 'applications',
    component: ApplicationsComponent
  },
  {
    path: 'post-job',
    component: CreateEditPostJobComponent
  },
  {
    path: 'find-teacher',
    component: FindTeacherComponent
  },
  {
    path: 'jobs-list',
    component: JobsListComponent
  },
  {
    path: 'job-details',
    component: JobDetailsComponent
  },
  {
    path: 'my-posts',
    component: MyPostsComponent
  },
  {
    path: 'teacher-follows-contact',
    component: TeacherFollowsContactComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
