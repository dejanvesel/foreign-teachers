import { AppService } from 'src/app/app.service';
import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';
import { AuthService } from 'src/app/auth/auth.service';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-jobs-list',
  templateUrl: './jobs-list.component.html',
  styleUrls: ['./jobs-list.component.scss']
})
export class JobsListComponent implements OnInit {
  

  constructor(private _appService : AppService, private _companyService: CompanyService, private _authService: AuthService) { }

  show1: boolean = false;
  public jobs: any;
  private _unsubscribeAll: Subject<void> = new Subject();
  public id : number;
  
  ngOnInit(): void {
    this._appService.defaultHeader = false;
    this._authService.activeUserData.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      data => {
        if (data) {
          this.getBriefJobs();
        }
      }
    );
  }
  readMore() {
    let dots = document.getElementById("dots");
    let moreText = document.getElementById("more");
    let btnText = document.getElementById("myBtn");
  
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "read more";
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "read less";
      moreText.style.display = "inline";
    }
  }
 //reloads current page 
  refresh(): void {
    window.location.reload();
}

getBriefJobs() {
  this._companyService.getBriefJobs().subscribe(
    data => {
      this.jobs = data;
      this.jobs = this.jobs.data.list
  })
}

showDetails(jobs, index) {
  this.id = jobs[index].jid;
  this._companyService.jobDetailsIndex.next(this.id);
}

ngOnDestroy(): void {
  this._unsubscribeAll.next();
  this._unsubscribeAll.complete();
}
}
