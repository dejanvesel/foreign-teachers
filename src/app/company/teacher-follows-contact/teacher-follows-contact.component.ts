import { AppService } from 'src/app/app.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-teacher-follows-contact',
  templateUrl: './teacher-follows-contact.component.html',
  styleUrls: ['./teacher-follows-contact.component.scss']
})
export class TeacherFollowsContactComponent implements OnInit {

  constructor(public _appService: AppService) {
    this._appService.titleChange.next("Teacher Follows & Contact");
    this._appService.defaultHeader = true;
   }

  ngOnInit(): void {
  }

}
