import { AppService } from './../../app.service';
import { Router } from '@angular/router';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { AuthService } from 'src/app/auth/auth.service';
import { Subject, takeUntil } from 'rxjs';
import { CompanyService } from '../company.service';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-my-posts',
  templateUrl: './my-posts.component.html',
  styleUrls: ['./my-posts.component.scss']
})
export class MyPostsComponent implements OnInit, OnDestroy {

  private _unsubscribeAll: Subject<void> = new Subject();
  public jobs: any;
  public id: number;
  public jid: number;
  public toogle = new FormControl('', []);
  public enableJob: boolean = false;

  constructor(private _appComponent: AppComponent, private _router: Router, public _appService : AppService, public _authService: AuthService, public _companyService: CompanyService) {
    this._appService.titleChange.next("My Posts");
    this._appService.defaultHeader = false;
  }

  ngOnInit(): void {
    // Koga pravis refresh na sajtot, mu treba vreme da go povika Login apito i da gi dobie podatocite
    // zatoa ne mozes odma da gi zemes, tuku pravis subscribe na ovoj subject, pa koga ke gi dobie podatocite prodolzuvas da gi koristis
    this._authService.activeUserData.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      data => {
        if (data) {
          this.getBriefJobs();
        }
      }
    );

    this.toogle.valueChanges.subscribe(newToogleValue=> {
      console.log("toogleValue", newToogleValue);
      this.enableJob = newToogleValue;
   });
  }

  getBriefJobs() {
    this._companyService.getBriefJobs().subscribe(
      data => {
        console.log("Get Brief Jobs",data)
        this.jobs = data;
        this.jobs = this.jobs.data.list
    })
  }

  showPost(jobs, index) {
    // this._appComponent.switchUp = true;
    this.id = index;
    this.jid = jobs[index].jid;

    this._router.navigate(
      ['../company/post-job'],
      { queryParams: { id: `${this.id}`, jid:  `${jobs[index].jid}`} }
    );
    this._companyService.enableJob(this.jid, this.enableJob).subscribe(
      data => {
        console.log(data)
      }
    )
  }

  showForm() {
    this._appComponent.showTheForm = true;
    this._router.navigate(['./company/post-job'])
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
