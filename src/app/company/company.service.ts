import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Subject } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { companyData } from './create-edit-post-job/create.model';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  jobDetailsIndex: Subject<any> = new Subject;

  constructor(private httpClient: HttpClient,
              private _authService : AuthService) { }


  createJob(obj: companyData) {
    let header = new HttpHeaders({
      'token': `${this._authService.loggedInToken}`,
      'current-region': 'jp',
    });
    return this.httpClient.post<companyData>(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/match/companies/${this._authService.loggedInData.data.auth.role_id}/jobs`, obj, {headers: header}).pipe(map(data => data));
  }

  getJob(index: any) {
    let header = new HttpHeaders({
      'current-region': 'jp',
      'token': `${this._authService.loggedInToken}`
    });
    return this.httpClient.get(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/match/companies/${this._authService.loggedInData.data.auth.role_id}/jobs/${index}`, {headers: header}).pipe(map(data => data));
  }

  getBriefJobs() {
    let header = new HttpHeaders({
      'current-region': 'jp',
      'token': `${this._authService.loggedInToken}`
    });
    return this.httpClient.get(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/match/companies/${this._authService.loggedInData.data.auth.role_id}/jobs/brief`, {headers: header}).pipe(map(data => data));
  }

  deleteJob(index: any) {
    let header = new HttpHeaders({
      'current-region': 'jp',
      'token': `${this._authService.loggedInToken}`
    });
    return this.httpClient.delete(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/match/companies/${this._authService.loggedInData.data.auth.role_id}/jobs/${index}`, {headers: header}).pipe(map(data => data));
  }

  updateJob(obj: companyData, index: any) {
    let header = new HttpHeaders({
      'current-region': 'jp',
      'token': `${this._authService.loggedInToken}`
    });
    return this.httpClient.put<companyData>(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/match/companies/${this._authService.loggedInData.data.auth.role_id}/jobs/${index}`, obj, {headers: header}).pipe(map(data => data));
  }

  enableJob( index: any, value: boolean) {
    let header = new HttpHeaders({
      'current-region': 'jp',
      'token': `${this._authService.loggedInToken}`
    });
    return this.httpClient.put(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/match/companies/${this._authService.loggedInData.data.auth.role_id}/jobs/${index}/enable/${value}`, null, {headers: header}).pipe(map(data => data));
  }
}
