import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyRoutingModule } from './company-routing.module';
import { FindTeacherComponent } from './find-teacher/find-teacher.component';
import { JobsListComponent } from './jobs-list/jobs-list.component';
import { TeacherFollowsContactComponent } from './teacher-follows-contact/teacher-follows-contact.component';
import { ApplicationsComponent } from './applications/applications.component';
import { MyPostsComponent } from './my-posts/my-posts.component';
import { CreateEditPostJobComponent } from './create-edit-post-job/create-edit-post-job.component';
import { SharedModule } from '../shared/shared.module';
import { JobDetailsComponent } from './job-details/job-details.component';



@NgModule({
  declarations: [
    FindTeacherComponent,
    JobsListComponent,
    TeacherFollowsContactComponent,
    ApplicationsComponent,
    MyPostsComponent,
    CreateEditPostJobComponent,
    JobDetailsComponent,
  ],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    SharedModule,
  ]
})
export class CompanyModule { }
