import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from './auth/authentication.guard';
import { FindTeacherComponent } from './company/find-teacher/find-teacher.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { FindJobComponent } from './teacher/find-job/find-job.component';
import { TeachersCardsComponent } from './teacher/teachers-cards/teachers-cards.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: LandingPageComponent
  },
  {
    path: 'teachers',
    component: TeachersCardsComponent
  },
  {
    path: 'find-job',
    component: FindJobComponent
  },
  {
    path: 'find-teacher',
    component: FindTeacherComponent
  },
  {
    path: 'company',
    loadChildren: () => import('./company/company.module').then(m => m.CompanyModule)
  },
  {
    path: 'teacher',
    loadChildren: () => import('./teacher/teacher.module').then(m => m.TeacherModule)
  },
  {
    path: 'account',
    loadChildren: () => import('./account-pages/account-pages.module').then(m => m.AccountPagesModule),
    canActivate: [AuthenticationGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
