import { AppComponent } from 'src/app/app.component';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor() { }

  titleChange: Subject<string> = new Subject<string>();
  defaultHeader: boolean = true;
  createAPost: boolean = false;
  isShow: boolean = false;
  isShowForm: boolean = true;
  isShowResume: boolean = true;
}
