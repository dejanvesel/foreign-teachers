import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-send-resume',
  templateUrl: './send-resume.component.html',
  styleUrls: ['./send-resume.component.scss']
})
export class SendResumeComponent implements OnInit {

  constructor( private dialogRef: MatDialogRef<SendResumeComponent>,
    @Inject(MAT_DIALOG_DATA) data,) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
