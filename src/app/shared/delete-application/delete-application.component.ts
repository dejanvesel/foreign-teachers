import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-delete-application',
  templateUrl: './delete-application.component.html',
  styleUrls: ['./delete-application.component.scss']
})
export class DeleteApplicationComponent implements OnInit {

  constructor( private dialogRef: MatDialogRef<DeleteApplicationComponent>,
     @Inject(MAT_DIALOG_DATA) private data: any,) { }

  ngOnInit(): void {
  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }
  
}
