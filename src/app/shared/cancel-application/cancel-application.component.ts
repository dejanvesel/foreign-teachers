import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-cancel-application',
  templateUrl: './cancel-application.component.html',
  styleUrls: ['./cancel-application.component.scss']
})
export class CancelApplicationComponent implements OnInit {

  constructor( private dialogRef: MatDialogRef<CancelApplicationComponent>,
    @Inject(MAT_DIALOG_DATA) data,) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
