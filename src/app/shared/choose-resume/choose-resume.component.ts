import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-choose-resume',
  templateUrl: './choose-resume.component.html',
  styleUrls: ['./choose-resume.component.scss']
})
export class ChooseResumeComponent implements OnInit {

  chosenResumeIndex : number;
  chosenResume : any;
  resumes = this._authService.loggedInData.data.match.brief_resumes;
  form = new FormGroup({
    resume: new FormControl('')
  });

  constructor( private dialogRef: MatDialogRef<ChooseResumeComponent>,
    @Inject(MAT_DIALOG_DATA) data, private _authService : AuthService) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  submit(){
    this.chosenResumeIndex = this.form.value.resume;
    this.chosenResume = this.resumes[this.chosenResumeIndex];
    this.dialogRef.close(this.chosenResumeIndex);
  }
}
