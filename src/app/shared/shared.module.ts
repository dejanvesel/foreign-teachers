import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { CloseAccountComponent } from './close-account/close-account.component';
import { SendResumeComponent } from './send-resume/send-resume.component';
import { ChooseResumeComponent } from './choose-resume/choose-resume.component';
import { ApplyJobCompletedComponent } from './apply-job-completed/apply-job-completed.component';
import { DeletePositionComponent } from './delete-position/delete-position.component';
import { CancelApplicationComponent } from './cancel-application/cancel-application.component';
import { DeleteResumeComponent } from './delete-resume/delete-resume.component';
import { SharePostComponent } from './share-post/share-post.component';
import { DeletePostComponent } from './delete-post/delete-post.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { NguCarouselModule } from '@ngu/carousel';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { CarouselComponent } from './carousel/carousel.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { PaginatorComponent } from './paginator/paginator.component';
import { StylePaginatorDirective } from './paginator/style-paginator.directive';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatExpansionModule } from '@angular/material/expansion';
import { DeleteApplicationComponent } from './delete-application/delete-application.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SpinnerComponent } from './spinner/spinner.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { ChooseResumeForJobApplyComponent } from './choose-resume-for-job-apply/choose-resume-for-job-apply.component';

@NgModule({
  declarations: [
    CloseAccountComponent,
    SendResumeComponent,
    ChooseResumeComponent,
    ApplyJobCompletedComponent,
    DeletePositionComponent,
    CancelApplicationComponent,
    DeleteResumeComponent,
    SharePostComponent,
    DeletePostComponent,
    CarouselComponent,
    PaginatorComponent,
    PaginatorComponent,
    StylePaginatorDirective,
    DeleteApplicationComponent,
    SpinnerComponent,
    ChooseResumeForJobApplyComponent,
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatIconModule,
    MatMenuModule,
    NguCarouselModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
  ],
  exports: [
    MatDialogModule,
    MatIconModule,
    MatMenuModule,
    FormsModule,
    ReactiveFormsModule,
    NguCarouselModule,
    MatTabsModule,
    CarouselComponent,
    MatPaginatorModule,
    PaginatorComponent,
    MatSlideToggleModule,
    MatTooltipModule,
    MatExpansionModule,
    DeleteApplicationComponent,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    SpinnerComponent,
  ],
  entryComponents: [PaginatorComponent],

  bootstrap: [PaginatorComponent],
})
export class SharedModule { }
