import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/auth/auth.service';
import { TeacherService } from 'src/app/teacher/teacher.service';

@Component({
  selector: 'app-delete-resume',
  templateUrl: './delete-resume.component.html',
  styleUrls: ['./delete-resume.component.scss']
})
export class DeleteResumeComponent implements OnInit {
  constructor( private dialogRef: MatDialogRef<DeleteResumeComponent>,
    @Inject(MAT_DIALOG_DATA) data, private _authService :  AuthService) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteResume(): void {
    this.dialogRef.close("ok");
  }

}
