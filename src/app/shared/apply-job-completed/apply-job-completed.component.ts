import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-apply-job-completed',
  templateUrl: './apply-job-completed.component.html',
  styleUrls: ['./apply-job-completed.component.scss']
})
export class ApplyJobCompletedComponent implements OnInit {

  constructor( private dialogRef: MatDialogRef<ApplyJobCompletedComponent>,
    @Inject(MAT_DIALOG_DATA) data,) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
