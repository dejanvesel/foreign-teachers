import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { logIn } from './app.model';
import { AuthService } from './auth/auth.service';
import { SpinnerComponent } from './shared/spinner/spinner.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  switchUp: boolean = false;
  showTheForm: boolean = false;
  title = 'foreign-teachers-app';


  logInObj: logIn = new logIn;
  dialogRef: any;

  constructor (
    public _router: Router,
    public _authService: AuthService,
    private dialog: MatDialog,
    ) {
  }

  ngOnInit(): void {
    this.checkActiveUser();

    this._authService.activeUserData.subscribe((data) => {
      if (data) {
        if (!this._authService.loggedInData) {
          this._authService.loggedInData = data;
        }
      }
    });
  }

  checkActiveUser() {
    let token = this._authService.getToken();
    if (token) {
      this.showSpinner();
      this.logInObj = JSON.parse(token);
      this._authService.logIn(this.logInObj).subscribe(data => {
        localStorage.setItem('FTeacherToken', JSON.stringify(this.logInObj));
        this._authService.loggedInData = data;
        this._authService.loggedInToken = this._authService.loggedInData.data.auth.token;
        this._authService.activeUserData.next(data);
        console.log('Active User:', this._authService.loggedInData);
        this._authService.loggedInRole.next(this._authService.loggedInData.data.auth.role);
        this.dialogRef.close();
        return true;
      }, error => {
        this.dialogRef.close();
        return false;
      });
    } else {
      if (this.dialogRef) {
        this.dialogRef.close();
      }

    }
  }

  showSpinner() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    let dialogWidth: string;
    let dialogHeight: string;
    let positionBottom: string;

    if (window.innerWidth < 768) {
      dialogWidth = '100%';
      dialogHeight = '100%';
      positionBottom = '0px';
    } else {
      dialogWidth = '780px';
      dialogHeight = '100%';
      positionBottom = '';
    }
    this.dialogRef = this.dialog.open(SpinnerComponent, {
      height: dialogWidth,
      width: dialogHeight,
      maxWidth: dialogWidth,
      maxHeight: '50vh',
      panelClass: 'spinner-custom',
      position: {
        bottom: positionBottom,
      }
    });
  }

}
