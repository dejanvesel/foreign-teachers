import { CreateEditResumeComponent } from './../teacher/create-edit-resume/create-edit-resume.component';
import { AuthService } from './../auth/auth.service';
import { AppService } from './../app.service';
import { AppComponent } from 'src/app/app.component';
import { Component, OnInit, AfterContentInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { LoginComponent } from '../auth/login/login.component';
import { SharedModule } from '../shared/shared.module';
import { delay, Subscription, timeout } from 'rxjs';
import { SpinnerComponent } from '../shared/spinner/spinner.component';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material/snack-bar';
import { logIn } from '../app.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterContentInit  {
  declarations: [
    SharedModule
  ]
  imports: [
    SharedModule
  ]
  exports: [
    SharedModule,

  ]
  mobMenu: boolean = true;
  show: boolean = true;
  title: string = "title";
  preview: boolean = true;
  showLogin: boolean = false;
  showUser: boolean = true;
  clickEventsubscription:Subscription;
  dialogRef2: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  logInObj: logIn = new logIn;
  role: boolean = false;
  user: any;

  constructor(
    private dialog: MatDialog,
    public _appService :AppService,
    private _authService : AuthService,
    private _snackBar: MatSnackBar,
    private _router: Router,
  ) {

    this._authService.activeUserData.subscribe((data) => {
      if (data) {
        this.showLoginUser();
        this.user = data;
        this.user = this.user.data.auth.role;
      }
      // if (this.user == 'company') {
      //   this.role = true;
      // } else if (this.user == 'teacher') {
      //   this.role = false;
      // }
    });

    this._authService.loggedInRole.subscribe((data) => {
      if(data === "company"){
        this.role = true;
      } else {
        this.role = false;
      }
    })
   }

  ngAfterContentInit(): void {
    this._appService.titleChange.subscribe(
      data => {
        if(data != undefined && data != null) {
          this.title = data;
        }
      }
    );
  }

  ngOnInit() {}

  logOut() {
    this.openDialog2();
    this._authService.logOut().subscribe(
      data => {
        if(data) {
          this.showLogin = false;
          this.showUser = true;
          localStorage.removeItem('FTeacherToken');
          localStorage.removeItem(JSON.stringify(this._authService.loggedInData));
          this.dialogRef2.close();
          this._authService.loggedInData = null;
          this._authService.activeUserData.next(null);
          this._router.navigate(['../home']);

          this._snackBar.open(`Successful sign-out!`, 'OK', {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          }).afterDismissed().pipe(delay(0)).subscribe(
            () => {
            }
          );
        }
      }
    )
  }

  openDialog2() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    let dialogWidth: string;
    let dialogHeight: string;
    let positionBottom: string;

    if (window.innerWidth < 768) {
      dialogWidth = '100%';
      dialogHeight = '100%';
      positionBottom = '0px';
    } else {
      dialogWidth = '780px';
      dialogHeight = '100%';
      positionBottom = '';
    }
    this.dialogRef2 = this.dialog.open(SpinnerComponent, {
      height: dialogWidth,
      width: dialogHeight,
      maxWidth: dialogWidth,
      maxHeight: '50vh',
      panelClass: 'spinner-custom',
      position: {
        bottom: positionBottom,
      }
    });
  }

  showLoginUser() {
      this.showLogin = true;
      this.showUser = false;
  };

  openDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    // dialogConfig.data = {
    //   id: 1,
    //   title: 'Angular For Beginners'
    // };
    let dialogWidth: string;
    let dialogHeight: string;

    if (window.innerWidth < 768) {
      dialogWidth = '100%';
      dialogHeight = '100%';
    } else {
      dialogWidth = '765px';
      dialogHeight = '100%';
    }

    const dialogRef = this.dialog.open(LoginComponent, {
      height: dialogWidth,
      width: dialogHeight,
      maxWidth: '600px',
      maxHeight: dialogHeight,
      panelClass: 'custom-modalbox'
    });

    // this.dialog.open(LoginComponent, {
    //   maxWidth: '100vw',
    //   maxHeight: '100vh',
    //   height: '100%',
    //   width: '100%',
    //   panelClass: 'custom-modalbox'
    // });
}

  toggleMobMenu() {
    this.mobMenu = !this.mobMenu;
    this.show = !this.show
  }
  close() {
    this.mobMenu = true;
    this.show = !this.show;
    this.preview = false;
  }

  // ngOnDestroy() {
  //   this._appService.titleChange.unsubscribe();
  // }
}
