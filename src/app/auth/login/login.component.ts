import { logIn, meta } from './../../app.model';
import { AuthService } from './../auth.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { SignUpComponent } from '../sign-up/sign-up.component';
import { delay, Subject, takeUntil } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { SpinnerComponent } from 'src/app/shared/spinner/spinner.component';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private _unsubscribeAll: Subject<void> = new Subject();
  email: FormControl = new FormControl();
  password: FormControl = new FormControl();
  logInObj: logIn = new logIn;
  wrongCredentials : boolean = false;
  registerForm: FormGroup;
  dialogRef2: any;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private dialogRef: MatDialogRef<LoginComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private dialog: MatDialog,
    private _authService : AuthService,
    private _formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    ) {
      this.registerForm = this._formBuilder.group({
        email: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
        });
    }

  ngOnInit(): void {
    this._authService.getPublicKey().subscribe(
      data1 => {
        this._authService.codeOne = data1;
        this._authService.codeOne = this._authService.codeOne.data;
      }
    );
  }

  clickLogIn() {
    this.openDialog2();
    this.logInObj.email = this.registerForm.get('email').value;;
    let _meta:meta = new meta('jp', 'teacher', this.registerForm.get('password').value);
    this.logInObj.meta = JSON.stringify(_meta);
    this.logInObj.pubkey = this._authService.codeOne;

    this._authService.logIn(this.logInObj).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      data => {
        if(data) {
          localStorage.setItem('FTeacherToken', JSON.stringify(this.logInObj));
          this.dialogRef.close();
          this.dialogRef2.close();
          this._authService.activeUserData.next(data);
          this._authService.loggedInData = data;
          this._authService.loggedInToken = this._authService.loggedInData.data.auth.token;
          console.log('Active User:', this._authService.loggedInData);
          this._authService.loggedInRole.next(this._authService.loggedInData.data.auth.role);
          this._snackBar.open(`Successful login!`, 'OK', {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          }).afterDismissed().pipe(delay(0)).subscribe(
            () => {
            }
          );
        }
      },
      error => {
        this._snackBar.open(`Oops! Something went wrong!`, 'OK', {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        }).afterDismissed().pipe(delay(0)).subscribe(
          () => {
            this.wrongCredentials = true;
            this.dialogRef2.close();
          }
        );
    }
    );

  }

  errorMsg() {
    let errormsg = document.getElementById('error');
    errormsg.style.display = "none";
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  openDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    // dialogConfig.data = {
    //   id: 1,
    //   title: 'Angular For Beginners'
    // };
    let dialogWidth: string;
    let dialogHeight: string;

    if (window.innerWidth < 768) {
      dialogWidth = '100%';
      dialogHeight = '100%';
    } else {
      dialogWidth = '780px';
      dialogHeight = '100%';
    }

    const dialogRef = this.dialog.open(SignUpComponent, {
      height: dialogWidth,
      width: dialogHeight,
      maxWidth: '600px',
      maxHeight: dialogHeight,
      panelClass: 'custom-modalbox'
    });


    this.dialogRef.close();
}
openDialog1() {

  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;

  // dialogConfig.data = {
  //   id: 1,
  //   title: 'Angular For Beginners'
  // };
  let dialogWidth: string;
  let dialogHeight: string;
  let positionBottom: string;

  if (window.innerWidth < 768) {
    dialogWidth = '100%';
    dialogHeight = '100%';
    positionBottom = '0px';

  } else {
    dialogWidth = '780px';
    dialogHeight = '100%';
    positionBottom = '';
    this.dialogRef.close();
  }

  const dialogRef = this.dialog.open(ForgotPasswordComponent, {
    height: dialogWidth,
    width: dialogHeight,
    maxWidth: dialogWidth,
    maxHeight: '50vh',
    panelClass: 'custom-modalbox',
    position: {
      bottom: positionBottom,
    }

  });


}

openDialog2() {
  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;

  let dialogWidth: string;
  let dialogHeight: string;
  let positionBottom: string;

  if (window.innerWidth < 768) {
    dialogWidth = '100%';
    dialogHeight = '100%';
    positionBottom = '0px';
  } else {
    dialogWidth = '780px';
    dialogHeight = '100%';
    positionBottom = '';
  }
  this.dialogRef2 = this.dialog.open(SpinnerComponent, {
    height: dialogWidth,
    width: dialogHeight,
    maxWidth: dialogWidth,
    maxHeight: '50vh',
    panelClass: 'spinner-custom',
    position: {
      bottom: positionBottom,
    }
  });
}


}
