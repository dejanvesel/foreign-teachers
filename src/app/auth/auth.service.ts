import { confirmSignUp, logIn } from './../app.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { signUpPost } from '../app.model';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  testObj: signUpPost = new signUpPost;
  confirmSignUpObj: confirmSignUp = new confirmSignUp;
  timestamp: number = 0;
  codeOne: any;
  codeTwo: string;
  loggedInData: any;
  loggedInToken: string;
  signUpConfirmData : any;
  teacherId : any;
  loggedInRole: BehaviorSubject<any> = new BehaviorSubject("teacher");

  // After every login (manually or on page refresh),
  // we provide the user data here and subscribe to this BehaviorSubject everywhere we need it
  public activeUserData: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(
    private httpClient: HttpClient,
    ) {
   }

  getToken() {
    let savedData = localStorage.getItem('FTeacherToken');
    if (typeof savedData !== 'undefined' && savedData !== '') {
      return savedData;
    }
    return null;
  }

  getPublicKey() {
    let header = new HttpHeaders({  'region': 'jp'  });
    return this.httpClient.get(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/auth/welcome?region`, {headers: header});
  }

  getSignUpCode(obj: signUpPost) {
    let header = new HttpHeaders({  'region': 'jp'  });
    return this.httpClient.post<signUpPost>(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/auth/signup`, obj, {headers: header}).pipe(map(data => data));
  }

  signUpConfirm(obj: confirmSignUp) {
    let header = new HttpHeaders({  'region': 'jp'  });
    return this.httpClient.post<signUpPost>(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/auth/signup/confirm`, obj, {headers: header}).pipe(map(data => data));
  }

  logIn(obj: logIn) {
    let header = new HttpHeaders({
      'current-region': 'jp',
      'region': 'jp'
    });
    return this.httpClient.post<logIn>(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/auth/login`, obj, {headers: header}).pipe(map(data => data));
  }

  logOut() {
    let header = new HttpHeaders({  'token': `${this.loggedInToken}`  });
    return this.httpClient.post<any>(`https://3s462xau8b.execute-api.ap-northeast-1.amazonaws.com/dev/api/v1/auth/logout`, this.loggedInData.data.auth.email, {headers: header}).pipe(map(data => data));
  }
}
