import { TeacherService } from './../../teacher/teacher.service';
import { AppService } from 'src/app/app.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '../auth.service';
import { LoginComponent } from '../login/login.component';
import { confirmSignUp, meta, signUpPost } from 'src/app/app.model';
import { delay, Subject, takeUntil } from 'rxjs';
import { SpinnerComponent } from 'src/app/shared/spinner/spinner.component';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit, OnDestroy {

  codeOne: any;
  codeTwo: any;
  test: any;
  registerForm: FormGroup;
  dialogRef2: any;


  testObj: signUpPost = new signUpPost;
  confirmSignUpObj: confirmSignUp = new confirmSignUp;
  private _unsubscribeAll: Subject<void> = new Subject();

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private dialogRef: MatDialogRef<SignUpComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private dialog: MatDialog,
    private _authService: AuthService,
    private _formBuilder: FormBuilder,
    private _appService: AppService,
    private _teacherService: TeacherService,
    private _snackBar: MatSnackBar
  ) {
    this.registerForm = this._formBuilder.group({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {
    this._authService.getPublicKey().subscribe(
      data1 => {
        this._authService.codeOne = data1;
        this._authService.codeOne = this._authService.codeOne.data;
      }
    );
  }

  saveData() {
    this.openDialog2();
    this.testObj.email = this.registerForm.get('email').value;
    let _meta:meta = new meta('jp', this.registerForm.value.role, this.registerForm.get('password').value);
    this.testObj.meta = JSON.stringify(_meta);
    console.log(this.registerForm.value.role);
    this._authService.getSignUpCode(this.testObj).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      data => {
        this.codeTwo = data;
        this.signupConfirm();
      },
      error => {
        // wrong format of credentials message
        this.dialogRef2.close();
    }
    );
  }

  signupConfirm() {
    this.confirmSignUpObj.email = this.registerForm.get('email').value;;
    this.confirmSignUpObj.confirm_code = JSON.stringify(this.codeTwo.data.for_testing_only);
    this.confirmSignUpObj.pubkey = this._authService.codeOne;

    this._authService.signUpConfirm(this.confirmSignUpObj).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      data3 => {
        this._snackBar.open(`Successful registration! Redirecting to Login.. `, 'OK', {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        }).afterDismissed().pipe(delay(0)).subscribe(
          () => {
            this.test = data3;
            this._authService.signUpConfirmData = data3;
            this._authService.teacherId = this._authService.signUpConfirmData.data.role_id;
            this.dialogRef2.close();
            this.openDialog();
            this.dialogRef.close();
          }
        );
      },
      error => {
        this._snackBar.open(`Oops! Something went wrong!`, 'OK', {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        }).afterDismissed().pipe(delay(0)).subscribe(
          () => {
          }
        );
      }
    )
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    let dialogWidth: string;
    let dialogHeight: string;

    if (window.innerWidth < 768) {
      dialogWidth = '100%';
      dialogHeight = '100%';
    } else {
      dialogWidth = '765px';
      dialogHeight = '100%';
    }

    const dialogRef = this.dialog.open(LoginComponent, {
      height: dialogWidth,
      width: dialogHeight,
      maxWidth: '600px',
      maxHeight: dialogHeight,
      panelClass: 'custom-modalbox'
    });

    this.dialogRef.close();
  }

  openDialog2() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    let dialogWidth: string;
    let dialogHeight: string;
    let positionBottom: string;

    if (window.innerWidth < 768) {
      dialogWidth = '100%';
      dialogHeight = '100%';
      positionBottom = '0px';
    } else {
      dialogWidth = '780px';
      dialogHeight = '100%';
      positionBottom = '';
    }
    this.dialogRef2 = this.dialog.open(SpinnerComponent, {
      height: dialogWidth,
      width: dialogHeight,
      maxWidth: dialogWidth,
      maxHeight: '50vh',
      panelClass: 'spinner-custom',
      position: {
        bottom: positionBottom,
      }
    });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
